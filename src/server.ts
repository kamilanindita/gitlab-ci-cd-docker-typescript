import express from 'express'
import httpStatus from 'http-status';

const app = express()
app.get('/', (req, res) => {
    res.status(httpStatus.OK).send('This App is running');
});

app.get('/hello/:name', (req, res) => {
    res.status(httpStatus.OK).json({
        message: 'OK',
        data: `Hello, ${req.params.name} !`
    });
})


const PORT = parseInt(process.env.PORT || '3000', 10);
const HOSTNAME = process.env.HOSTNAME || '0.0.0.0';
app.listen(PORT, HOSTNAME, () => {
    console.log(`This app running on ${HOSTNAME}:${PORT}`)
})
export default app